{-# LANGUAGE TypeFamilies, TypeOperators, FlexibleContexts #-}

module Meeting1 ((<~>)(..), RoseTree) where

{-
 - Code based on the Leuven Haskell User Group talk "Haskell: A Universe of Types".
 -
 - Tom Schrijvers
 -}

-- * Isomorphism

data a <~> b
  =  Iso { to :: a -> b
         , from :: b -> a }

-- Round-Trip Laws:
--   to iso . from iso = id
--   from iso . to iso = id

-- * Generic Universe

data Zero

data One
  =  One
  deriving Show

-- Ex1: Show that One is isomorphic to the built-in `unit' type
--         data () = ()

data (x + y)
  =  Inl x
  |  Inr y
  deriving Show

-- Ex2: Show that (+) is isomorphic to the predefined Either type
-- 		data Either x y  =  Left x | Right y

data (x * y)
  =  x :*: y
  deriving Show

-- Ex3: Show that (*) is isomorphic to the built-in tuple type
-- 		data (x,y) = (x,y)


-- * Arithmethic Laws

-- x + 0 = x
rightZero :: (x + Zero) <~> x
rightZero = Iso t f where
  t (Inl x) =  x
  f x = Inl x

-- (x + y) + z = x + (y + z)
assocPlus :: ((x + y) + z) <~> (x + (y + z))
assocPlus = Iso t f where
  t (Inl (Inl x))  =  Inl x
  t (Inl (Inr y))  =  Inr (Inl y)
  t (Inr z)        =  Inr (Inr z)

  f = undefined -- Ex4

-- Ex5: write the isomorphisms for
--
--  (a)  x + y = y + x
--  (b)  x * 1 = x
--  (c)  x * y = y * x
--  (d)  x * (y * z) = (x * y) * z
--  (e)  x * (y + z) = x * y + x * z

-- Ex6:
--   (a)  Come up with a way to compose isomorphisms (a <~> b) and (b <~> c)
--   (b)  Use this composition to create an isomorphism for
--        the arithmetic law: 0 + x = x

-- Ex7: Come up with a definition for the identity isomorhism  (a <~> a)

-- Ex8:
--   (a) Come up with a way to promote isomorphisms (a <~> b) to
--       isomorphisms (a + c <~> b + c) and also to isomorphisms (a * c <~> b * c).
--   (b) Use this to show that:
--       (x + y) + z <~> (y + x) + z

-- Ex9:
--   Write the data type for (x ^ y) and derive the ismorphism with functions


-- Ex10: write the isomorphisms for
--
--  (a)  x ^ (y + z) = (x ^ y) * (x ^ z)
--  (b)  x ^ (y * z) = x ^ y ^ z
--  (c)  (x * y) ^ z = (x ^ z) * (y ^ z)

-- Ex11: write Ex10 (b) in a different way
--   using a combination of Ex3
--   and the Prelude functions curry and uncurry
--   as well as some glue code.

-- Ex18: write the isomorphism   (Bool -> Int) <~> (Int * Int)

-- * Generic Representation

class Representable a where
  type Rep a

  repIso :: a <~> Rep a

toRep :: Representable a => a -> Rep a
toRep = to repIso

fromRep :: Representable a => Rep a -> a
fromRep = from repIso


instance Representable Bool where
  type Rep Bool  =  One + One

  repIso = Iso t f where
    t True   =  Inl One
    t False  =  Inr One

    f (Inl One)  =  True
    f (Inr One)  =  False


instance Representable (Maybe a) where
  type Rep (Maybe a)  =  One + a

  repIso = Iso t f where
    t Nothing   =  Inl One
    t (Just x)  =  Inr x

    f (Inl One)  =  Nothing
    f (Inr x)    =  Just x

instance Representable [a] where
  type Rep [a]  =  One + (a * [a])

  repIso = Iso t f where
    t []      =  Inl One
    t (x:xs)  =  Inr (x :*: xs)

    f (Inl One)         =  []
    f (Inr (x :*: xs))  =  x : xs

-- Ex12: create an instance for the predefined Prelude type
--         data Ordering = LT | EQ | GT

-- Ex13: create an instance for
data Tree a  =  Empty | Leaf a | Fork (Tree a) (Tree a)


-- Ex14: create an instance for
data RoseTree a  =  Node a [RoseTree a]


-- * Generic Equality

class GEq a where
  geq :: a -> a -> Bool

-- * Equality for Universe

instance GEq One where
  geq One One  =  True

instance (GEq x, GEq y) => GEq (x + y) where
  geq (Inl x1) (Inl x2)  =  geq x1 x2
  geq (Inr y1) (Inr y2)  =  geq y1 y2
  geq _        _         =  False

instance (GEq x, GEq y) => GEq (x * y) where
  geq (x1 :*: y1) (x2 :*: y2)  =  geq x1 x2 && geq y1 y2

-- * Equality for Primitive Types

instance GEq Int where
  geq = (==)

instance GEq Char where
  geq = (==)

-- * Equality for Representable Types

deriveGEq :: (Representable a, GEq (Rep a)) => a -> a -> Bool
deriveGEq x y  =  geq (toRep x) (toRep y)

instance GEq Bool where
  geq  =  deriveGEq

instance GEq a => GEq (Maybe a) where
  geq  =  deriveGEq

instance GEq a => GEq [a] where
  geq  =  deriveGEq

-- Ex14: add instances for
--  (a)  Ordering
--  (b)  Tree a
--  (c)  RoseTree a

-- Ex15: Create a generic definition for ordering
class GEq a => GOrd a where
  gcompare :: a -> a -> Ordering

-- Ex16: Create a generic definition for serialisation.
--       We use a list of booleans to represent the serialised bitstream.
class Serialisable a where
  serialise    ::  a -> [Bool]
  deserialise  ::  [Bool] -> a

-- Ex17: Advanced
--   Figure out how to serialise to and from a "ByteString" instead of [Bool].
--   See the bytestring and bytestring-builder packages on Hackage.
